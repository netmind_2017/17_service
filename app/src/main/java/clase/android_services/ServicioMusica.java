package clase.android_services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.provider.Settings;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

public class ServicioMusica extends Service {

    MediaPlayer reproductor;

    @Override
    public void onCreate() {
        Toast.makeText(this,"Servicio creado",Toast.LENGTH_SHORT).show();
        reproductor = MediaPlayer.create(this, R.raw.shakira);
    }

    @Override
    public int onStartCommand(Intent intenc, int flags, int idArranque) {
        Toast.makeText(this,"Servicio arrancado "+ idArranque,Toast.LENGTH_SHORT).show();
        reproductor.start();

        /*Thread one = new Thread() {
            public void run() {
                try {
                    System.out.println("Does it work?");
                    while(true) {
                        System.out.println("yes");
                        Thread.sleep(3 * 1000);
                    }
                } catch(InterruptedException v) {
                    System.out.println(v);
                }
            }
        };
        one.start();*/




        /*
        //TIMER
        timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            //CLEAN LIST
                            adapter = new ItemAdapter(mContext,android.R.layout.simple_list_item_1,util.contactList);
                            util.contactList.clear();
                            MainActivity.list.setAdapter(null);

                            new GetContacts(mContext,MainActivity.this).execute("http://api.androidhive.info/contacts/");

                        } catch (Exception e) {
                            Log.d(TAG ,e.getMessage());
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 30000); //execute 1 minute
        * */

        /*
          Retorna describe cómo ha de comportarse el sistema cuando el proceso del servicio sea matado una vez que el servicio ya se ha inicializado. Esto puede ocurrir en situaciones de baja memoria. Los siguientes valores están permitidos:

            START_STICKY: Cuando sea posible el sistema tratará de recrear el servicio, se realizará una llamada a onStartCommand() pero con el parámetro intencion igual a null. Esto tiene sentido cuando el servicio puede arrancar sin información adicional, como por ejemplo, el servicio mostrado para la reproducción de música de fondo.

            START_NOT_STICKY: El sistema no tratará de volver a crear el servicio, por lo tanto el parámetro intencion nunca podrá ser igual a null. Esto tiene sentido cuando el servicio no puede reanudarse una vez interrumpido.

            START_REDELIVER_INTENT: El sistema tratará de volver a crear el servicio. El parámetro intencion será el que se utilizó en la última llamada startService(Intent).

            START_STICKY_COMPATIBILITY: Versión compatible de START_STICKY, que no garantiza que onStartCommand() sea llamado después de que el proceso sea matado.


         */


        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this,"Servicio detenido",Toast.LENGTH_SHORT).show();
        reproductor.stop();
    }

    @Override
    public IBinder onBind(Intent intencion) {
        return null;
    }


}
